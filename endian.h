/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#ifndef ENDIAN_H
#define ENDIAN_H

typedef unsigned char byte;


static inline int get_u16_be(const void *p) {
  const byte *c = (const byte*)p;
  return (c[0] << 8) | c[1];
}

static inline int get_u32_be(const void *p) {
  const byte *c = (const byte*)p;
  return (c[0] << 24) | (c[1] << 16) | (c[2] << 8) | c[3];
}

static inline int get_u16_le(const void *p) {
  const byte *c = p;
  return c[0] | (c[1] << 8);
}

static inline int get_u32_le(const void *p) {
  const byte *c = p;
  return c[0] | (c[1] << 8) | (c[2] << 16) | (c[3] << 24);
}


#endif  // ENDIAN_H
