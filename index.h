/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@param antonio.cz>
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include <stdint.h>
#include "common.h"

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif


#define FLAG_UNIQUE          0x01
#define FLAG_FOR_CLAUSE      0x08
#define FLAG_COMPACT         0x20
#define FLAG_COMPOUNDING     0x40
#define FLAG_STRUCTURE       0x80

#define ROOT_NODE            0x01
#define LEAF_NODE            0x02

#define NOT_FOUND            -2


#pragma pack(push, 1)

// Index file header
typedef struct {
    uint32_t    root_node;          // bytes 0-3
    uint32_t    free_node;          // bytes 4-7
    uint32_t    version;            // bytes 8-11
    short   	key_len;            // bytes 12-13
    char    	flags;              // byte 14
    char    	signature;          // byte 15
} index_header_t;

// Index node header
typedef struct {
    short   	type;               // bytes 0-1
    short   	key_count;          // bytes 2-3
    uint32_t    left_sibling;       // bytes 4-7
    uint32_t    right_sibling;      // bytes 8-11
} node_t;

// Compressed index node header 
typedef struct {
    short   	free_space;         // bytes 12-13
    uint32_t    record_number_mask; // bytes 14-17
    char    	dup_count_mask;     // byte 18
    char    	trail_count_mask;   // byte 19
    char    	record_number_bits; // byte 20
    char    	dup_count_bits;     // byte 21
    char    	trail_count_bits;   // byte 22
    char    	bytes_total;        // byte 23
} compressed_node_t;

#pragma pack(pop)


#define NODE_SIZE        512
#define SC_BUF_SIZE     (NODE_SIZE - sizeof(node_t) - sizeof(compressed_node_t))

#ifdef _MSC_VER
#pragma warning(disable:4200)
#endif

typedef struct {
    uint32_t record_number;
    uint32_t dup_count;
    uint32_t trail_count;
} short_mask_t;

typedef struct {
    uint64_t record_number;
    uint64_t dup_count;
    uint64_t trail_count;
} long_mask_t;

// Search context (intialized in dbf_idx_find(), used by dbf_idx_next())
typedef struct {
    int key_len;            // Maximum key length
    char filler;            // Fill character for trailing bytes in key value
    int node_pos;          // Current index node position
    int next_node_pos;     // Next index node position
    int key_count;          // Key count in current node
	// Decompressing parameters
    int bytes_total;
	union {
		short_mask_t s;
		long_mask_t l;
	} mask;
    int dup_count_shr;
    int trail_count_shr;
    int idx_pos;
    int key_pos;
    char buf[SC_BUF_SIZE];  // Current node payload
    char key[];             // Current key value
} search_context_t;


// Index description
typedef struct {
    char* name;
	char expr[512];
    int key_len;
    int root_node;
} dbf_index_t;


// Index file handle
typedef struct {
    dbf_index_t* indices;
    int index_count;
    search_context_t* last_search;
    FILE* file;
} dbf_compound_index_t;


/** 
 * Create new search context
 *
 * @param key_len	Maximum key length
 * @param filler	Fill character. Will be used to fill trailing bytes from
 *              	the key end to the buffer end
 *
 * @return			Search context
 */
search_context_t* dbf_create_search_context(int key_len, char filler);


/** 
 * Read compressed node.
 *
 * @param sc		Search context
 * @param f			Index file
 *
 * @return			TRUE on success
 */
bool dbf_uncompress_init(search_context_t* sc, FILE* f);


/** 
 * Move to next key in compressed node and update the search context accordingly.
 *
 * @param sc		Search context
 *
 * @return			Next key position in index file or -1 at node end
 */
int dbf_uncompress_next(search_context_t* sc);


/***
 * Move to next key in compressed node if it's same as the current key.
 * Update the search context accordingly
 *
 * @param sc		Search context
 *
 * @return			Next key position in index file or -1 if the next key is different
 * 					or at node end
 */
int dbf_uncompress_next_if_same(search_context_t* sc);


/** 
 * Open index file.
 *
 * @param tablename	Table name
 * @param dir		Directory with table files. Must end with slash. If NULL, the current working directory is used.
 *
 * @return			Index file handle or -1 on error
 */
dbf_compound_index_t* dbf_open_index(const char* tablename, const char* dir);


/** 
 * Free index handle and close index file.
 *
 * @param cdx		Index file handle
 */
void dbf_close_index(dbf_compound_index_t* cdx);


/** 
 * Get index ID by its name
 *
 * @param cdx		Index file name
 * @param name		Index name
 *
 * @return			Index ID or -1 if no such index exists
 */
int dbf_idx_get_index_id(dbf_compound_index_t* cdx, const char* name);


/** 
 * Find the first occurence of @param value in index @id. 
 * 
 * @param cdx		Index file handle
 * @param id		Index ID
 * @param value		Search value
 *
 * @return	 		Row number if the value was found
 * 					-2 if the value was not found
 * 					-1 on error
 */
int dbf_idx_find(dbf_compound_index_t* cdx, int index_id, const char* value);


/** 
 * Find the next occurence of @param value previously found by dbf_idx_find().
 * Call this function only after dbf_idx_find()
 *
 * @param cdx		Index file handle
 *
 * @return	 		Row number if the value was found
 * 					-2 if the value was not found
 * 					-1 on error
 */
int dbf_idx_next(dbf_compound_index_t* cdx);


#ifdef __cplusplus
}
#endif
