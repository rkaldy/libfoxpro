/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@param antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#ifndef FOXPRO_H
#define FOXPRO_H


#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif


// Error codes
#define ERR_DBF_FILENAME_TOO_LONG   101
#define ERR_DBF_FILE_NOT_FOUND		102
#define ERR_DBF_BAD_VERSION			103
#define ERR_DBF_BAD_FORMAT			104
#define ERR_DBF_INVALID_RECORD_ID   105
#define ERR_DBF_INVALID_FIELD_ID	106
#define ERR_DBF_INVALID_FIELD_TYPE  107
#define ERR_DBF_NO_RECORD_SELECTED  108
#define ERR_DBF_RECORD_DELETED		109
#define ERR_DBF_READ_ERROR			110
#define ERR_DBF_READ_EOF			111
#define ERR_DBF_BAD_INDEX_TYPE		120
#define ERR_DBF_FIND_NEXT			121
#define ERR_DBF_INVALID_INDEX_ID	122
#define ERR_DBF_NO_INDEX			123


typedef enum {
	FIELD_CHAR,
	FIELD_CURRENCY,
	FIELD_NUMERIC,
	FIELD_FLOAT,
	FIELD_DATE,
	FIELD_DATETIME,
	FIELD_DOUBLE,
	FIELD_INTEGER,
	FIELD_BOOLEAN,
	FIELD_MEMO,
	FIELD_GENERAL,
	FIELD_PICTURE,
	FIELD_ERROR = -1
} col_type_t;


typedef struct dbf_file_t* dbf_handle_t;


/**
 * Get last error code
 */
int dbf_last_error();

/***
 * Get additional description of the last error
 */
char* dbf_last_error_desc();

/** 
 * Open all files belonging to the table @param tablename (table, memo and index file)
 *
 * @param tablename		Table name
 * @param dir			Directory with table files. Must end with slash. If NULL, the current working directory is used.
 *
 * @return				New table handle, -1 on error
 */
dbf_handle_t dbf_open(const char* tablename, const char* dir);

/** 
 * Free the table handle and close all table files (table, memo and index file)
 *
 * @param dbf			Table handle
 */
void dbf_close(dbf_handle_t dbf);

/** 
 * Get table name
 *
 * @param dbf			Table handle
 */
const char* dbf_get_name(dbf_handle_t dbf);

/** 
 * Get total table row count
 *
 * @param dbf			Table handle
 */
int dbf_get_row_count(dbf_handle_t dbf);

/** 
 * Get column count
 *
 * @param dbf			Table handle
 */
int dbf_get_col_count(dbf_handle_t dbf);

/** 
 * Get index count
 *
 * @param dbf			Table handle
 */
int dbf_get_index_count(dbf_handle_t dbf);

/** 
 * Get cursor position (counted from zero)
 *
 * @param dbf			Table handle
 */
int dbf_get_pos(dbf_handle_t dbf);

/** 
 * Move cursor to the given row
 *
 * @param dbf			Table handle
 * @param pos			Row number (counted from zero)
 *
 * @return				0 if ok, -1 on error
 */
int dbf_seek(dbf_handle_t dbf, int pos);

/** 
 * Return TRUE if the current row is marked as deleted
 *
 * @param dbf			Table handle
 */
bool dbf_is_deleted(dbf_handle_t dbf);

/***
 * Find column by name
 *
 * @param dbf			Table handle
 * @param col_name		Column name
 *
 * @return				Column ID or -1 if no such column found
 */
int dbf_get_col_id(dbf_handle_t dbf, const char* col_name);

/** 
 * Get column name
 *
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 *
 * @return				Column name or -1 if column ID is out of range
 */
const char* dbf_get_col_name(dbf_handle_t dbf, int col_id);

/** 
 * Get column type
 *
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 *
 * @return				Column type	or -1 if column ID is out of range
 */
col_type_t dbf_get_col_type(dbf_handle_t dbf, int col_id);

/** 
 * Read value from @param col_id> column of current row to the <buf>. At most <buf_len> bytes will be stored.
 * If column is NOT marked as binary, a trailing '\0' is appended to the buffer.
 * This is a raw read function for strings and blobs. Use dbf_read_XXX for read specific data type.
 * 
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 * @param buf			Output buffer
 * @param buf_len		Output buffer length
 *
 * @return				Number of bytes read or -1 on error
 */
int dbf_read(dbf_handle_t dbf, int col_id, char* buf, size_t buf_len);

/** 
 * Read integer value from <col_id> column of current row.
 * 
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 * @param value			Pointer to the output value
 *
 * @return				Number of bytes read or -1 on error
 */
int dbf_read_int(dbf_handle_t dbf, int col_id, int* value);

/** 
 * Read double value from <col_id> column of current row.
 * 
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 * @param value			Pointer to the output value
 *
 * @return				Number of bytes read or -1 on error
 */
int dbf_read_double(dbf_handle_t dbf, int col_id, double* value);

/** 
 * Read boolean value from <col_id> column of current row.
 * 
 * @param dbf			Table handle
 * @param col_id		Column ID (counted from zero)
 * @param value			Pointer to the output value
 *
 * @return				Number of bytes read or -1 on error
 */
int dbf_read_bool(dbf_handle_t dbf, int col_id, bool* value);


/** 
 * Find table index by name
 *
 * @param dbf			Table handle
 * @param index_name	Index name
 *
 * @return				Index ID or -1 if no such index found
 */
int dbf_get_index_id(dbf_handle_t dbf, const char* index_name);

/** 
 * Get index name
 *
 * @param dbf			Table handle
 * @param index_id		Index ID
 *
 * @return	 			Index name or -1 if index ID is out of range
 */
const char* dbf_get_index_name(dbf_handle_t dbf, int index_id);

/** 
 * Get the indexed expression
 *
 * @param dbf			Table handle
 * @param index_id		Index ID
 *
 * @return	 			Indexed expression or -1 if index ID is out of range
 */
const char* dbf_get_index_expr(dbf_handle_t dbf, int index_id);

/** 
 * Find the first occurence of @param value in column @id. If the value exists in some
 * row, move cursor to the row. Otherwise set cursor to -1;
 * 
 * @param dbf			Table handle
 * @param id			Column ID or index ID (both counted from zero)
 * @param use_index		if TRUE, the @id is index ID, otherwise column ID
 * @param value			search value
 *
 * @return	 			1 if value was found, 0 if not found, -1 on error
 */
int dbf_find(dbf_handle_t dbf, int id, bool use_index, const void* value);

/** 
 * Find the next occurence of @param value previously found by dbf_find(). If the value 
 * exists in some row, move cursor to the row. Otherwise set cursor to -1;
 * Call this function only after dbf_find()
 *
 * @param dbf			Table handle
 *
 * @return	 			1 if value was found, 0 if not found, -1 on error
 */
int dbf_find_next(dbf_handle_t dbf);

/** 
 * Iterate through all table rows.
 * 
 * Syntax:
 *     DBF_FOR_ALL(dbf) {
 *         commands ... (use dbf_read_XXX() for read values from actual row)
 *     }
 */
#define DBF_FOR_ALL(dbf)													\
	do {																	\
		for (int rowid = 0; rowid < dbf_get_row_count(dbf); rowid++) {		\
			dbf_seek(dbf, rowid);											\
			if (dbf_is_deleted(dbf)) continue;								\

#define DBF_END_FOR }} while (0)


/** 
 * Iterate through all table rows with specific column value.
 * Parameters are same as for dbf_find():
 *
 * @param 1				Table handle
 * @param 2				Column ID or index ID (both counted from zero)
 * @param 3				if TRUE, the @id is index ID, otherwise column ID
 * @param 4				search value
 *
 * Syntax:
 *     DBF_FOR_ALL_FOUND(dbf, 4, 1, "xyz") {
 *         commands ... (use dbf_read_XXX() for read values from actual row)
 *     } DBF_END_FOR_FOUND(dbf);
 */
#define DBF_FOR_ALL_FOUND(dbf, id, use_index, key)							\
	do {																	\
		int found = dbf_find(dbf, id, use_index, key);						\
		while (found) {														

#define DBF_END_FOR_FOUND(dbf)												\
			found = dbf_find_next(dbf);										\
		}																	\
	} while (0)


#ifdef __cplusplus
}
#endif

#endif  // FOXPRO_H
