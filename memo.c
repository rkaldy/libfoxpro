/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "foxpro.h"
#include "common.h"
#include "endian.h"
#include "memo.h"


#pragma pack(push, 1)

// Memo file header
typedef struct {
	uint32_t	free_block;			// bytes 0-3
	short		unused;				// bytes 4-5
	short		block_size;			// bytes 6-7
} memo_header_t;

// Memo block header
typedef struct {
	uint32_t 	type;				// bytes 0-3
	uint32_t 	size;				// bytes 4-7
} memo_block_header_t;

#pragma pack(pop)


dbf_memo_t* dbf_open_memo(const char* tablename, const char* dir) {
	dbf_memo_t* memo = (dbf_memo_t*)calloc(sizeof(dbf_memo_t), 1);
	memo_header_t header;

	if ((memo->file = dbf_open_file(tablename, dir, MEMOFILE)) == NULL) {
		return (dbf_memo_t*)ERROR;
	}
	EWP(sfread(&header, sizeof(memo_header_t), memo->file), dbf_memo_t*);
	// Integers in der Memodatei sind als Big-Endian gespeichert (obwohl die Tabelledatei verwendt Little-Endian...wunderlich)
	memo->block_size = get_u16_be(&header.block_size);
	memo->block_count = get_u32_be(&header.free_block) - 1;
	return memo;
}


void dbf_close_memo(dbf_memo_t* memo) {
	if (memo->file) fclose(memo->file);
	free(memo);
}


int dbf_get_memo_field(dbf_memo_t* memo, int pos, bool binary, char* buf, size_t buf_len) {
	int len;
	memo_block_header_t header;

	// Read block header
	fseek(memo->file, memo->block_size * pos, SEEK_SET);
	EW(sfread(&header, sizeof(memo_block_header_t), memo->file));

	// Read block contents
	if (binary) {
		len = MIN(get_u32_be(&header.size), buf_len - 1);
		EW(sfread(buf, len, memo->file));
		buf[len] = '\0';
		return len;
	} 
	else {
		len = MIN(get_u32_be(&header.size), buf_len - 1);
		EW(sfread(buf, len, memo->file));
		len = trim(buf, len);
		return len;
	}
}
