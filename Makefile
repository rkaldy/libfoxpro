CC = gcc
DFLAGS = -g -D_DEBUG
#DFLAGS = -O2
CFLAGS = --std=c99 -Wall -fPIC -D_GNU_SOURCE $(DFLAGS)
LFLAGS = -shared
OUTFILE = libfoxpro.so
PREFIX = /usr/local

include Makefile.common

install: $(OUTFILE)
	chmod 644 $(OUTFILE)
	cp $(OUTFILE) $(PREFIX)/lib
	cp foxpro.h $(PREFIX)/include

uninstall:
	rm -f $(PREFIX)/lib/$(OUTFILE)
	rm -f $(PREFIX)/include/foxpro.h
