/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include "foxpro.h"
#include "common.h"


int dbf_errno;
char dbf_errdesc[BUFSIZE];
static const char* EXT[] = { "dbf", "cdx", "fpt" };


void strtoupper(char* str) {
	for (size_t i = 0; i < strlen(str); i++) {
		str[i] = toupper(str[i]);
	}
}


void strtolower(char* str) {
	for (size_t i = 0; i < strlen(str); i++) {
		str[i] = tolower(str[i]);
	}
}


int trim(char* str, int len) {
	while ((len > 0) && (str[len - 1] == ' ')) len--;
	str[len] = '\0';
	return len;
}


void set_error(int code, dbf_handle_t dbf, const char* descfmt, ...) {
	char* p = dbf_errdesc;
	dbf_errno = code;
	
	if (dbf) {
		p += snprintf(p, BUFSIZE - 1, "table=%s", dbf_get_name(dbf));
	}
	if (descfmt) {
		va_list args;
		va_start(args, descfmt);
		*p++ = ' ';
		p += vsnprintf(p, BUFSIZE - (p - dbf_errdesc), descfmt, args);
		va_end(args);
	}
	*p = '\0';
}


bool sfread(void* buf, int size, FILE* file) {
	if (fread(buf, 1, size, file) != size) {
		if (feof(file)) {
			set_error(ERR_DBF_READ_EOF, NULL, NULL);
		} else {
			set_error(ERR_DBF_READ_ERROR, NULL, strerror(errno));
		}
		return ERROR;
	}
	return OK;
}


FILE* dbf_open_file(const char* tablename, const char* dir, file_type_t filetype) {
	if (strlen(tablename) + strlen(dir) + 5 > DBF_PATH_MAX) {		// 5 = 1 (dot) + 3 (extension) + 5 (trailing null)
		set_error(ERR_DBF_FILENAME_TOO_LONG, NULL, "table=%s dir=%s", tablename, dir);
		return NULL;
	}

	char filename[DBF_PATH_MAX];
	char* p;
	FILE* file;

	if (dir) {
		strcpy(filename, dir);
		p = filename + strlen(filename);
	} else {
		filename[0] = '\0';
		p = filename;
	}
	strcat(filename, tablename);
	strcat(filename, ".");
	strcat(filename, EXT[filetype]);
	
	strtolower(p);
	file = fopen(filename, "rb");
	if (!file) {
		strtoupper(p);
		file = fopen(filename, "rb");
		if (!file) {
			set_error(ERR_DBF_FILE_NOT_FOUND, NULL, "file=%s", filename);
			return NULL;
		}
	}
	return file;
}
