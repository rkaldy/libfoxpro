/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@param antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif


#define DBF_PATH_MAX	256
#define BUFSIZE			1024

#define OK				0
#define TRUE			1
#define FALSE			0
#define ERROR			-1


// Error wrapper. If the inner statement returns ERROR, interrupt the current function and return ERROR too
#define EW(stmt)		if ((stmt) == ERROR) return ERROR
#define EWP(stmt, type)	if ((int)(stmt) == ERROR) return (type)ERROR

#define MIN(a, b) (((a) < (b)) ? (a) : (b))


// Column description
typedef struct {
	char name[11];
	char type;
	int offset;
	int size;
	int decimals;
	bool binary;
} column_t;


typedef enum { TABLEFILE, INDEXFILE, MEMOFILE } file_type_t;


// Last error description
extern int dbf_errno;
extern char dbf_errdesc[];


/** 
 * Convert the @param str string to uppercase
 */
void strtoupper(char* str);


/** 
 * Convert the @param str string to lowercase
 */
void strtolower(char* str);


/** 
 * Delete trailing space at the end of string and set '\0' after the string last nonspace character.
 * The string need not to have trailing '\0'.
 *
 * @param str		String
 * @param len		String length
 *
 * @return			Trimmed string length
 */
int trim(char* str, int len);


/** 
 * Set dbf_errno and dbf_errdesc variables
 *
 * @param code		Error code
 * @param dbf		Table handle (can be NULL)
 * @param desc		Error description, can use printf-like format directives (can be NULL)
 */
void set_error(int code, dbf_handle_t dbf, const char* fmtdesc, ...);


/** 
 * fread() wrapper with error handling. If an error occurs, set dbf_error and dbf_errdesc.
 *
 * @param buf		Buffer for read bytes
 * @param size		Number of maximum bytes read
 * @param file		File handle
 *
 * @return			TRUE on success, FALSE on error
 */
bool sfread(void* buf, int size, FILE* file);


/** 
 * Open table index or memo file for @param tablename. The files have format @tablename.[dbf|cdx|fpt]
 * and the name can be in uppercase or lowercase.
 * 
 * @param tablename	Table name
 * @param dir		Search directory. If NULL, search in current working directory.
 * @param filetype	TABLEFILE, INDEXFILE oder MEMOFILE
 *
 * @return			File handle or NULL on error
 */
FILE* dbf_open_file(const char* tablename, const char* dir, file_type_t filetype);


#ifdef __cplusplus
}
#endif

#endif  // COMMON_H
