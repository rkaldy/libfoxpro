/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include "foxpro.h"
#include "common.h"
#include "memo.h"
#include "index.h"


#define MAGIC_FOXBASE 				0x02
#define MAGIC_DBASE3				0x03
#define MAGIC_FOXPRO				0x30
#define MAGIC_FOXPRO_AUTOINCREMENT	0x31
#define MAGIC_DBASE4_SQL			0x43
#define MAGIC_DBASE4_SYSTEM			0x63
#define MAGIC_DBASE3_MEMO			0x83
#define MAGIC_DBASE4_MEMO			0x8B
#define MAGIC_DBASE4_SQL_MEMO		0xCB
#define MAGIC_FOXPRO_2_MEMO			0xF5

#define BACKLINK_SECTION_SIZE		263

#define FLAG_HAS_INDEX		0x01
#define FLAG_HAS_MEMO		0x02
#define FLAG_RECORD_OK		0x20
#define FLAG_RECORD_DELETED	0x2A
#define FLAG_FIELD_BINARY	0x04


const char FIELD_TYPE[] = "CYNFDTBILMGP";


typedef struct {
	char	year;
	char	month;
	char	day;
} hdate_t;

#pragma pack(push, 1)

typedef struct {
	char		version;			// byte 0
	hdate_t		last_update;		// bytes 1-3
	uint32_t	row_count;			// bytes 4-7
	short		first_row;			// bytes 8-9
	short		row_size;			// bytes 10-11
	char		reserved_1[16];		// bytes 12-27
	char		flags;				// byte 28
	char		codepage;			// byte 29
	short		reserved_2;			// bytes 30-31
} header_t;

typedef struct {
	char		name[11];			// bytes 0-10
	char		type;				// byte 11
	uint32_t	offset;				// bytes 12-15
	char		size;				// byte 16
	char		decimals;			// byte 17
	char		flags;				// byte 18
	uint32_t	ai_next;			// bytes 19-22
	char		ai_step;			// byte 23
	char		reserved[8];		// bytes 24-31
} col_desc_t;

#pragma pack(pop)

typedef struct dbf_file_t {
    char* name;
    int col_count;
    int row_count;
	int first_row;
    int row_size;
	int pos;

    column_t* cols;
    char* row;
    FILE* file;

	int last_find_col_id;
	bool last_find_use_index;
	char* last_find_value;

	dbf_memo_t* memo;
	dbf_compound_index_t* index;
} dbf_file_t;


int dbf_last_error() {
	return dbf_errno;
}


char* dbf_last_error_desc() {
	return dbf_errdesc;
}


dbf_handle_t dbf_open(const char* tablename, const char* dir) {
	dbf_handle_t dbf = (dbf_handle_t)malloc(sizeof(dbf_file_t));
	header_t header;
	col_desc_t col_desc;

	dbf->file = NULL;
	dbf->index = NULL;
	dbf->memo = NULL;
	
	if ((dbf->file = dbf_open_file(tablename, dir, TABLEFILE)) == NULL)
	{
		return (dbf_handle_t)ERROR;
	}
	dbf->name = strdup(tablename);
	
	// File structure:
	//   header (32 bytes) 
	//   column descriptions (n * 32 bytes) 
	//   column description terminator (1 byte) 
	//   backlinks (263 bytes)
	//   data

	// Read table header
	EWP(sfread(&header, sizeof(header_t), dbf->file), dbf_handle_t);
	if (header.version != MAGIC_FOXPRO && header.version != MAGIC_FOXPRO_AUTOINCREMENT) {
		set_error(ERR_DBF_BAD_VERSION, dbf, "version=%02x", header.version);
		return (dbf_handle_t)ERROR;
	}
	dbf->row_count = header.row_count;
	dbf->first_row = header.first_row;
	dbf->row_size  = header.row_size;

	// Read column definitions
	dbf->col_count = (dbf->first_row - (sizeof(header_t) + BACKLINK_SECTION_SIZE + 1)) / sizeof(col_desc_t);
	dbf->cols = (column_t*)calloc(dbf->col_count, sizeof(column_t));
	for (int i = 0; i < dbf->col_count; i++) {
		EWP(sfread(&col_desc, sizeof(col_desc_t), dbf->file), dbf_handle_t);
		strcpy(dbf->cols[i].name, col_desc.name);
		dbf->cols[i].type 	= col_desc.type;
		dbf->cols[i].offset	= col_desc.offset;
		dbf->cols[i].size 	= col_desc.size;
		dbf->cols[i].decimals= col_desc.decimals;
		dbf->cols[i].binary	= col_desc.flags & FLAG_FIELD_BINARY;
	}

	// Read memo header
	if (header.flags & FLAG_HAS_MEMO) {
		dbf->memo = dbf_open_memo(tablename, dir);
	}

	// Read index
	if (header.flags & FLAG_HAS_INDEX) {
		dbf->index = dbf_open_index(tablename, dir);
	}
	
	dbf->row = (char*)malloc(dbf->row_size);
	dbf->pos = -1;		// Initial cursor position is just before the first row
	dbf->last_find_col_id = -1;
	dbf->last_find_value = NULL;

	return dbf;
}


void dbf_close(dbf_handle_t dbf) {
	if (dbf->file) fclose(dbf->file);
	if (dbf->memo) dbf_close_memo(dbf->memo);
	if (dbf->index) dbf_close_index(dbf->index);
	free(dbf->name);
	free(dbf->cols);
	free(dbf->row);
	free(dbf);
}


const char* dbf_get_name(dbf_handle_t dbf) {
	return dbf->name;
}


int dbf_get_row_count(dbf_handle_t dbf) {
	return dbf->row_count;
}


int dbf_get_col_count(dbf_handle_t dbf) {
	return dbf->col_count;
}


int dbf_get_index_count(dbf_handle_t dbf) {
	return dbf->index ? dbf->index->index_count : 0;
}


int dbf_get_pos(dbf_handle_t dbf) {
	return dbf->pos;
}


int dbf_seek(dbf_handle_t dbf, int pos) {
	if (pos < 0 || pos >= dbf->row_count) {
		set_error(ERR_DBF_INVALID_RECORD_ID, dbf, "row=%i", pos);
		return ERROR;
	}
	fseek(dbf->file, dbf->first_row + pos * dbf->row_size, SEEK_SET);
	EW(sfread(dbf->row, dbf->row_size, dbf->file));
	if (dbf->row[0] != FLAG_RECORD_OK && dbf->row[0] != FLAG_RECORD_DELETED) {
		set_error(ERR_DBF_BAD_FORMAT, dbf, "row=%i", pos);
		return ERROR;
	}
	dbf->pos = pos;
	return OK;
}


bool dbf_is_deleted(dbf_handle_t dbf) {
	return dbf->row[0] == FLAG_RECORD_DELETED;
}


int dbf_check_col(dbf_handle_t dbf, int col_id) {
	if (dbf->pos == -1) {
	// The cursor doesn't point to any row
		set_error(ERR_DBF_NO_RECORD_SELECTED, dbf, NULL);
		return ERROR;
	}
	if (col_id < 0 || col_id >= dbf->col_count) {
	// Field ID out of range
		set_error(ERR_DBF_INVALID_FIELD_ID, dbf, "col=%i", col_id);
		return ERROR;
	}
	if (dbf_is_deleted(dbf)) {
	// The current row is marked as deleted
		set_error(ERR_DBF_RECORD_DELETED, dbf, "row=%i", dbf->pos);
		return ERROR;
	}
	return OK;
}


int dbf_check_col_type(dbf_handle_t dbf, column_t* col, col_type_t type) {
	if (col->type != FIELD_TYPE[type]) {
		set_error(ERR_DBF_INVALID_FIELD_TYPE, dbf, "col=%s type=%c required=%c", col->name, col->type, FIELD_TYPE[type]);
		return ERROR;
	}
	return OK;
}


int dbf_get_col_id(dbf_handle_t dbf, const char* col_name) {
	for (int i = 0; i < dbf->col_count; i++) {
		if (strcasecmp(col_name, dbf->cols[i].name) == 0) {
			return i;
		}
	}
	set_error(ERR_DBF_INVALID_FIELD_ID, dbf, "col=%s", col_name);
	return ERROR;
}


const char* dbf_get_col_name(dbf_handle_t dbf, int col_id) {
	if (col_id < 0 || col_id >= dbf->col_count) {
		set_error(ERR_DBF_INVALID_FIELD_ID, dbf, "col=%i", col_id);
		return (const char*)ERROR;
	}
	return dbf->cols[col_id].name;
}


col_type_t dbf_get_col_type(dbf_handle_t dbf, int col_id) {
	EWP(dbf_check_col(dbf, col_id), col_type_t);

	char type = dbf->cols[col_id].type;
	col_type_t type_id = (col_type_t)(strchr(FIELD_TYPE, type) - FIELD_TYPE);
	if (type_id) {
		return type_id;
	} else {
		set_error(ERR_DBF_BAD_FORMAT, dbf, "row=%i col=%s type=%c", dbf->pos, dbf->cols[col_id].name, dbf->cols[col_id].type);
		return FIELD_ERROR;
	}
}


int dbf_read(dbf_handle_t dbf, int col_id, char* buf, size_t buf_len) {
	EW(dbf_check_col(dbf, col_id));
	column_t* col = &dbf->cols[col_id];

	if (col->type == FIELD_TYPE[FIELD_MEMO] || col->type == FIELD_TYPE[FIELD_PICTURE]) {
		int pos = *(uint32_t*)(dbf->row + col->offset);
		return dbf_get_memo_field(dbf->memo, pos, col->binary, buf, buf_len);
	}
	else if (col->binary) {
		int len = MIN(col->size, buf_len);
		memcpy(buf, dbf->row + col->offset, len);
		return len;
	}
	else {
		int len = MIN(col->size, buf_len - 1);
		memcpy(buf, dbf->row + col->offset, len);
		len = trim(buf, len);
		return len;
	}
}


int dbf_read_int(dbf_handle_t dbf, int col_id, int* value) {
	EW(dbf_check_col(dbf, col_id));
	column_t* col = &dbf->cols[col_id];
	EW(dbf_check_col_type(dbf, col, FIELD_INTEGER));

	*value = *(uint32_t*)(dbf->row + col->offset);
	return 4;
}


int dbf_read_double(dbf_handle_t dbf, int col_id, double* value) {
	EW(dbf_check_col(dbf, col_id));
	column_t* col = &dbf->cols[col_id];
	EW(dbf_check_col_type(dbf, col, FIELD_DOUBLE));

	*value = *(double*)(dbf->row + col->offset);
	return 8;
}


int dbf_read_bool(dbf_handle_t dbf, int col_id, bool* value) {
	EW(dbf_check_col(dbf, col_id));
	column_t* col = &dbf->cols[col_id];
	EW(dbf_check_col_type(dbf, col, FIELD_BOOLEAN));

	char c = *(dbf->row + col->offset);
	*value = (c == 'T' || c == 't' || c == 'Y' || c == 'y');
	return 1;
}


int dbf_get_index_id(dbf_handle_t dbf, const char* index_name) {
	return dbf_idx_get_index_id(dbf->index, index_name);
}


const char* dbf_get_index_name(dbf_handle_t dbf, int index_id) {
	if (!dbf->index || index_id < 0 || index_id >= dbf->index->index_count) {
		return (const char*)ERROR;
	}
	return dbf->index->indices[index_id].name;
}


const char* dbf_get_index_expr(dbf_handle_t dbf, int index_id) {
	if (!dbf->index || index_id < 0 || index_id >= dbf->index->index_count) {
		return (const char*)ERROR;
	}
	return dbf->index->indices[index_id].expr;
}


int dbf_full_table_scan(dbf_handle_t dbf, int col_id, const void* value) {
	char buf[BUFSIZE];

	for (int i = dbf->pos + 1; i < dbf->row_count; i++) {
		EW(dbf_seek(dbf, i));
		if (!dbf_is_deleted(dbf)) {
			EW(dbf_read(dbf, col_id, buf, BUFSIZE));
			if (strcmp(buf, value) == 0) {
				return TRUE;
			}
		}
	}
	dbf->pos = -1;
	return FALSE;
}


int dbf_find(dbf_handle_t dbf, int id, bool use_index, const void* value) {
	if (use_index) {
		if (!dbf->index) {
			set_error(ERR_DBF_NO_INDEX, dbf, NULL);
			return ERROR;
		}
		if (id < 0 || id > dbf->index->index_count) {
			set_error(ERR_DBF_INVALID_INDEX_ID, dbf, "index=%i", id);
			return ERROR;
		}
		int pos;
		dbf->last_find_use_index = true;
		EW(pos = dbf_idx_find(dbf->index, id, value));
		if (pos == NOT_FOUND) {
			dbf->pos = -1;
			return FALSE;
		}
		dbf_seek(dbf, pos - 1);
		if (dbf_is_deleted(dbf)) {
			return dbf_find_next(dbf);
		} else return TRUE;
	} else {
		dbf->last_find_col_id = id;
		if (dbf->last_find_value) free(dbf->last_find_value);
		dbf->last_find_value = strdup(value);
		dbf->last_find_use_index = false;
		dbf->pos = -1;
		return dbf_full_table_scan(dbf, id, value);
	}
}


int dbf_find_next(dbf_handle_t dbf) {
	if (dbf->last_find_use_index) {
		if (!dbf->index) {
			set_error(ERR_DBF_NO_INDEX, dbf, NULL);
			return ERROR;
		}
		if (!dbf->index->last_search) {
			set_error(ERR_DBF_FIND_NEXT, dbf, NULL);
			return ERROR;
		}
		int pos;
		do {
			EW(pos = dbf_idx_next(dbf->index));
			if (pos == NOT_FOUND) {
				dbf->pos = -1;
				return FALSE;
			}
			dbf_seek(dbf, pos - 1);
		} while (dbf_is_deleted(dbf));
		return TRUE;
	} else {
		if (dbf->last_find_col_id == -1) {
			set_error(ERR_DBF_FIND_NEXT, dbf, NULL);
			return ERROR;
		} else {
			return dbf_full_table_scan(dbf, dbf->last_find_col_id, dbf->last_find_value);
		}
	}
}
