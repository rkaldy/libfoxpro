# libfoxpro

The library provides read-only access to .dbf files of Foxpro databases.
You can seek in the table, read all basic data types and memos, search
the value with full table scan or with an index.

I wrote this library six years ago when I need to convert data from an ancient
legacy database. Don't know how it works with different Foxpro versios, use at
your own risk :-)

## Installation
### On Linux

The installation is short, simple and 100% autoconf-free:

	make
	make install

Optionally you can edit the `PREFIX` variable in `Makefile`.

### On Windows

I provide DLL and import library in `build.win32` and `build.win64` directory.
To cross-compile the library using Mingw:
	
	make -f Makefile.win32

or

	make -f Makefile.win64

## Usage

Use this include:

	#include <dbfread.h>

and link with:

	gcc -lfoxpro ...

## API

See `foxpro.h` source comments.

## License

This library is licensed under BSD license, see `LICENSE.txt`.
