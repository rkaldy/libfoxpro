/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "foxpro.h"
#include "common.h"
#include "endian.h"
#include "index.h"


search_context_t* dbf_create_search_context(int key_len, char filler) {
	search_context_t* sc;	

	sc = (search_context_t*)malloc(sizeof(search_context_t) + key_len + 1);
	sc->key[key_len] = '\0';
	sc->key_len = key_len;
	sc->filler = filler;
	return sc;
}


bool dbf_uncompress_init(search_context_t* sc, FILE* f) {
	compressed_node_t header;

	EW(sfread(&header, sizeof(compressed_node_t), f));
	EW(sfread(sc->buf, SC_BUF_SIZE, f));
	sc->bytes_total        = header.bytes_total;
	if (header.bytes_total <= 4) {
		sc->mask.s.record_number = header.record_number_mask;
		sc->mask.s.dup_count     = header.dup_count_mask << header.record_number_bits;
		sc->mask.s.trail_count   = header.trail_count_mask << (header.record_number_bits + header.dup_count_bits);
	} else {
		sc->mask.l.record_number = (uint64_t)header.record_number_mask;
		sc->mask.l.dup_count     = (uint64_t)header.dup_count_mask << header.record_number_bits;
		sc->mask.l.trail_count   = (uint64_t)header.trail_count_mask << (header.record_number_bits + header.dup_count_bits);
	}
	sc->dup_count_shr      = header.record_number_bits;
	sc->trail_count_shr	   = header.record_number_bits + header.dup_count_bits;
	sc->key_pos            = SC_BUF_SIZE;
	sc->idx_pos            = 0;
	return TRUE;
}


int dbf_uncompress_next(search_context_t* sc) {
	int dup_count, trail_count, len;
	int pos;

	if (sc->bytes_total <= 4) {
		uint32_t idx = *(uint32_t*)(sc->buf + sc->idx_pos);
		dup_count   = (idx & sc->mask.s.dup_count) >> sc->dup_count_shr;
		trail_count = (idx & sc->mask.s.trail_count) >> sc->trail_count_shr;
		pos = idx & sc->mask.s.record_number;
	} else {
		uint64_t idx = *(uint64_t*)(sc->buf + sc->idx_pos);
		dup_count   = (idx & sc->mask.l.dup_count) >> sc->dup_count_shr;
		trail_count = (idx & sc->mask.l.trail_count) >> sc->trail_count_shr;
		pos = idx & sc->mask.l.record_number;
	}

	sc->idx_pos += sc->bytes_total;
	sc->key_pos -= sc->key_len - dup_count - trail_count;
	len = sc->key_len - trail_count;
	memcpy(sc->key + dup_count, sc->buf + sc->key_pos, len - dup_count);
	memset(sc->key + len, sc->filler, sc->key_len - len);
	return pos;
}


int dbf_uncompress_next_if_same(search_context_t* sc) {
	int dup_count, trail_count;
	int pos;

	if (sc->bytes_total <= 4) {
		uint32_t idx = *(uint32_t*)(sc->buf + sc->idx_pos);
		dup_count   = (idx & sc->mask.s.dup_count) >> sc->dup_count_shr;
		trail_count = (idx & sc->mask.s.trail_count) >> sc->trail_count_shr;
		pos = idx & sc->mask.s.record_number;
	} else {
		uint64_t idx = *(uint64_t*)(sc->buf + sc->idx_pos);
		dup_count   = (idx & sc->mask.l.dup_count) >> sc->dup_count_shr;
		trail_count = (idx & sc->mask.l.trail_count) >> sc->trail_count_shr;
		pos = idx & sc->mask.l.record_number;
	}
	if (dup_count + trail_count == sc->key_len) {
		sc->idx_pos += sc->bytes_total;
		return pos;
	} else {
		return -1;
	}
}


dbf_compound_index_t* dbf_open_index(const char* tablename, const char* dir) {
	dbf_compound_index_t* cdx = (dbf_compound_index_t*)calloc(sizeof(dbf_compound_index_t), 1);
	index_header_t header;
	node_t node;
	search_context_t* sc;
	int idx_pos;
	int i;
	
	// Read file header
	if ((cdx->file = dbf_open_file(tablename, dir, INDEXFILE)) == NULL) {
		return (dbf_compound_index_t*)ERROR;
	}
	EWP(sfread(&header, sizeof(index_header_t), cdx->file), dbf_compound_index_t*);
	if (!(header.flags & FLAG_COMPACT) || !(header.flags & FLAG_COMPOUNDING)) {
		set_error(ERR_DBF_BAD_INDEX_TYPE, NULL, "table=%s cdx_flags=%02x", tablename, header.flags);
		return (dbf_compound_index_t*)ERROR;
	}

	// Index names are stored in the same way as keys in one index
	// Read root node
	fseek(cdx->file, header.root_node, SEEK_SET);
	EWP(sfread(&node, sizeof(node_t), cdx->file), dbf_compound_index_t*);
	cdx->index_count = node.key_count;
	cdx->indices = (dbf_index_t*)malloc(sizeof(dbf_index_t) * node.key_count);

	// Read indices
	sc = dbf_create_search_context(header.key_len, '\0');
	EWP(dbf_uncompress_init(sc, cdx->file), dbf_compound_index_t*);
	for (i = 0; i < node.key_count; i++) {
		idx_pos = dbf_uncompress_next(sc);
		cdx->indices[i].name = strndup(sc->key, sc->key_len);

		// Read index root node
		fseek(cdx->file, idx_pos, SEEK_SET);
		EWP(sfread(&header, sizeof(index_header_t), cdx->file), dbf_compound_index_t*);
		if (!(header.flags & FLAG_COMPACT) || !(header.flags & FLAG_COMPOUNDING)) {
			set_error(ERR_DBF_BAD_INDEX_TYPE, NULL, "table=%s idx_flags=%02x", tablename, header.flags);
			return (dbf_compound_index_t*)ERROR;
		}
		cdx->indices[i].root_node = header.root_node;
		cdx->indices[i].key_len = header.key_len;

		fseek(cdx->file, idx_pos + 0x200, SEEK_SET);
		EWP(sfread(cdx->indices[i].expr, 512, cdx->file), dbf_compound_index_t*);
	}
	free(sc);

	return cdx;
}


void dbf_close_index(dbf_compound_index_t* cdx) {
	if (cdx->file) fclose(cdx->file);
	if (cdx->last_search) free(cdx->last_search);
	for (int i = 0; i < cdx->index_count; i++) {
		free(cdx->indices[i].name);
	}
	free(cdx->indices);
	free(cdx);
}


int dbf_idx_get_index_id(dbf_compound_index_t* cdx, const char* name) {
	for (int i = 0; i < cdx->index_count; i++) {
		if (strcasecmp(name, cdx->indices[i].name) == 0) {
			return i;
		}
	}
	return ERROR;
}


int dbf_idx_find(dbf_compound_index_t* cdx, int index_id, const char* value) {
	dbf_index_t* index = &cdx->indices[index_id];
	uint32_t node_pos, record_pos, child_pos;
	node_t node;
	char *pat, *key;
	search_context_t* sc;
	int i;

	if (cdx->last_search) free(cdx->last_search);
	cdx->last_search = NULL;

	node_pos = index->root_node;
	int len = strlen(value);
	if (len == 0 || len > index->key_len) {
		return NOT_FOUND;
	}

	// Fill trailing bytes in search value by spaces
	pat = (char*)malloc(index->key_len + 1);
	key = (char*)malloc(index->key_len + 1);
	strcpy(pat, value);
	memset(pat + len, ' ', index->key_len - len);
	sc = dbf_create_search_context(index->key_len, ' ');

	// Traverse through index tree, until reach a leaf node
	do {
		fseek(cdx->file, node_pos, SEEK_SET);
		EW(sfread(&node, sizeof(node_t), cdx->file));
		if (node.type & LEAF_NODE) {
			break;
		}
		for (i = 0; i < node.key_count; i++) {
			fread(key, 1, index->key_len, cdx->file);
			fread(&record_pos, 4, 1, cdx->file);
			fread(&child_pos, 4, 1, cdx->file);
			if (strncmp(key, pat, index->key_len) >= 0) {
				node_pos = get_u32_be(&child_pos);
				break;
			}
		}
		if (i == node.key_count) {
			free(pat); free(key); free(sc);
			return NOT_FOUND;
		}
	} while (1);
	free(key);

    // Traverse through all keys in current leaf node
	do {
		EW(dbf_uncompress_init(sc, cdx->file));
		for (int i = 0; i < node.key_count; i++) {
			record_pos = dbf_uncompress_next(sc);
			int cmp = strncmp(sc->key, pat, index->key_len);
			if (cmp == 0) {
				sc->node_pos = node_pos;
				sc->next_node_pos = node.right_sibling;
				sc->key_count = node.key_count;
				cdx->last_search = sc;
				free(pat);
				return record_pos;
			} else if (cmp > 0) {
				free(pat); free(sc);
				return NOT_FOUND;
			}
		}
		node_pos = node.right_sibling;
		if (node_pos == -1) {
			free(pat); free(sc);
			return NOT_FOUND;
		}
		fseek(cdx->file, node_pos, SEEK_SET);
		EW(sfread(&node, sizeof(node_t), cdx->file));
	} while (1);
}


int dbf_idx_next(dbf_compound_index_t* cdx) {
	search_context_t* sc = cdx->last_search;

	if (sc->idx_pos == sc->key_count * sc->bytes_total) {
		// If the last key found was at the end of node, move to next node
		
		if (sc->next_node_pos == -1) {
			free(cdx->last_search);
			cdx->last_search = NULL;
			return NOT_FOUND;
		}

		node_t node;
		char* key = (char*)strndup(sc->key, sc->key_len);
		int record_pos;

		fseek(cdx->file, sc->next_node_pos, SEEK_SET);
		EW(sfread(&node, sizeof(node_t), cdx->file));
		sc->node_pos = sc->next_node_pos;
		sc->next_node_pos = node.right_sibling;
		sc->key_count = node.key_count;
		EW(dbf_uncompress_init(sc, cdx->file));
		record_pos = dbf_uncompress_next(sc);
		if (strncmp(sc->key, key, sc->key_len) != 0) {
			free(key);
			free(cdx->last_search);
			cdx->last_search = NULL;
			return NOT_FOUND;
		} else {
			free(key);
			return record_pos;
		}
	} else {
		// Otherwise look for next key in the node, whether is same as the current key

		int pos = dbf_uncompress_next_if_same(sc);
		if (pos == -1) {
			free(cdx->last_search);
			cdx->last_search = NULL;
			return NOT_FOUND;
		} else {
			return pos;
		}
	}
}
