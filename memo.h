/*
 * libfoxpro – Foxpro table reader library
 * 
 * Copyright (c) 2009, Robert Káldy <robert@param antonio.cz>
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE.txt file for details.
 */

#include "common.h"

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif


// Memo file handle
typedef struct {
    int block_size;
    int block_count;
    FILE* file;
} dbf_memo_t;


/** 
 * Open memo file.
 *
 * @param tablename	Table name
 * @param dir		Directory with table files. Must end with slash. If NULL, the current working directory is used.
 *
 * @return			Memo file handle or -1 on error
 */
dbf_memo_t* dbf_open_memo(const char* tablename, const char* dir);


/** 
 * Free memo handle and close memo file.
 *
 * @param memo		Memo file handle
 */
void dbf_close_memo(dbf_memo_t* memo);


/** 
 * Read memo field contents. At most @param buf_len bytes will be read.
 * 
 * @param memo		Memo file handle
 * @param pos		Memo position in the file
 * @param binary	If FALSE, trim trailing spaces. Otherwise leave data unchanged.
 * @param buf		Target buffer
 * @param buf_len	Target buffer length
 *
 * @return			Number of bytes read or -1 on error
 */
int dbf_get_memo_field(dbf_memo_t* memo, int pos, bool binary, char* buf, size_t buf_len);


#ifdef __cplusplus
}
#endif
